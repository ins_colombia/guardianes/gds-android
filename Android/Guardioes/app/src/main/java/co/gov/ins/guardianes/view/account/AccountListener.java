package co.gov.ins.guardianes.view.account;

import co.gov.ins.guardianes.model.User;

/**
 * @author Igor Morais
 */
public interface AccountListener {

    void onCancel();

    void onError();

    void onNotFound(User user);

    void onSuccess(User user);
}
