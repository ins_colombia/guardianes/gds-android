package co.gov.ins.guardianes.model;

public enum PointType {

    HOSPITAL, PHARMACY;
}
