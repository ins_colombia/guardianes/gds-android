package co.gov.ins.guardianes.request.base;

public final class RequesterConfig {

    public static final int TIMEOUT = 25;
    //private static final String ADDRESS = "http://164.41.147.241"; // Centeias
    private static final String ADDRESS = "http://201.234.75.175"; // External
    //private static final String ADDRESS = "http://10.10.100.232"; // Internal
    private static final String ADDRESS_DEV = ADDRESS; // External
    private static final String PORT = "";
    private static final String CONTEXT = "";
    public static final String URL_PROD = ADDRESS + CONTEXT;
    public static final String URL_DEV = ADDRESS_DEV + CONTEXT;
    public static final String URL = URL_DEV;

    public static class Api {

        public static final String USER = "/user";
        public static final String NOTICE = "/news";
    }
}
