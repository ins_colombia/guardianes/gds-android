package co.gov.ins.guardianes.view.menu;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatEditText;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import org.parceler.Parcels;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;
import co.gov.ins.guardianes.R;
import co.gov.ins.guardianes.helper.CitiesHelp;
import co.gov.ins.guardianes.helper.Constants;
import co.gov.ins.guardianes.helper.DateFormat;
import co.gov.ins.guardianes.helper.DialogBuilder;
import co.gov.ins.guardianes.helper.Helper;
import co.gov.ins.guardianes.helper.Mask;
import co.gov.ins.guardianes.manager.PrefManager;
import co.gov.ins.guardianes.model.Country;
import co.gov.ins.guardianes.model.User;
import co.gov.ins.guardianes.request.UserRequester;
import co.gov.ins.guardianes.request.base.AuthRequester;
import co.gov.ins.guardianes.request.base.RequestHandler;
import co.gov.ins.guardianes.request.base.RequestListener;
import co.gov.ins.guardianes.view.CountryAdapter;
import co.gov.ins.guardianes.view.HomeActivity;
import co.gov.ins.guardianes.view.base.BaseAppCompatActivity;

public class UserActivity extends BaseAppCompatActivity {

    private static final String COLOMBIA = "CO";

    @Bind(R.id.text_view_message)
    TextView textViewMessage;

    @Bind(R.id.edit_lastname)
    AppCompatEditText editTextLastname;

    @Bind(R.id.edit_firstname)
    AppCompatEditText editTextFirstname;

    @Bind(R.id.edit_text_parent_other)
    AppCompatEditText editTextParent;

    @Bind(R.id.edit_text_birth_date)
    AppCompatEditText editTextBirthDate;

    @Bind(R.id.text_view_state)
    TextView textViewState;

    @Bind(R.id.spinner_gender)
    Spinner spinnerGender;

    @Bind(R.id.text_view_race)
    TextView textViewRace;

    @Bind(R.id.text_view_city)
    TextView textViewCity;

    @Bind(R.id.spinner_race)
    Spinner spinnerRace;

    @Bind(R.id.spinner_parent)
    Spinner spinnerParent;

    @Bind(R.id.spinner_country)
    Spinner spinnerCountry;

    @Bind(R.id.spinner_state)
    Spinner spinnerState;

    @Bind(R.id.spinner_city)
    Spinner spinnerCity;

    private User user;
    private boolean main;
    private boolean create;
    private CountryAdapter spinnerAdapter;
    private Country colombia = null;

    @Override
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);

        setContentView(R.layout.user);

        main = getIntent().getBooleanExtra(Constants.Bundle.MAIN_MEMBER, false);
        user = Parcels.unwrap(getIntent().getParcelableExtra(Constants.Bundle.USER));
        setCreate(user == null);

        loadView();
        for (Country obj : spinnerAdapter.getContries()) {
            if (obj.getName().equalsIgnoreCase("Colombia"))
                colombia = obj;
        }

        spinnerCountry.post(new Runnable() {
            @Override
            public void run() {
                spinnerCountry.setSelection(spinnerAdapter.getPosition(colombia));
            }
        });

        if (create) {
            user = new User();

        } else {
            load(user);
        }
    }

    private void load(final User user) {
        Log.d("USER load", user.toString());
        editTextLastname.setText(user.getLastname());
        editTextFirstname.setText(user.getFirstname());

        spinnerGender.setSelection(user.getGender().equalsIgnoreCase("Masculino") ? 1 : 2);

        List<String> races = toList(getResources().getStringArray(R.array.race_array));
        for (int i = 0; i < races.size(); i++) {
            if (races.get(i).equals(user.getRace())) {
                spinnerRace.setSelection(i);
                break;
            }

        }

        List<String> relationship = toList(getResources().getStringArray(R.array.relationship_array));
        Log.d("Selected spinner id:", String.valueOf(spinnerParent.getSelectedItemId()));
        Log.d("Selected spinner value:", String.valueOf(spinnerParent.getSelectedItem()));

        for (int i = 0; i < relationship.size(); i++) {
            if (relationship.get(i).equals(user.getRelationship())) {
                spinnerParent.setSelection(i);
                break;
            }

        }

        if (spinnerParent.getSelectedItemId() == 0) {
            spinnerParent.setSelection(relationship.size() - 1);
            editTextParent.setText(user.getRelationship());
        }

        final String format = DateFormat.getDate(user.getDob(), "dd/MM/yyyy");

        editTextBirthDate.setText(format);

        if (user.getCountry() != null) {

            for (int i = 0; i < spinnerAdapter.getCount(); i++) {

                Country country = spinnerAdapter.getItem(i);
                if (country != null && user.getCountry().equals(country.getName())) {
                    final int finalI = i;
                    spinnerCountry.post(new Runnable() {
                        @Override
                        public void run() {
                            spinnerCountry.setSelection(finalI);
                        }
                    });

                    Log.d("Country", ((Country) spinnerCountry.getSelectedItem()).getName());
                }

            }

            if (user.getCountry().equalsIgnoreCase(colombia.getName())) {
                List<String> states = CitiesHelp.getState(toList(getResources().getStringArray(R.array.array_state_city)));
                String state_selected = "";
                for (int i = 0; i < states.size(); i++) {
                    if (states.get(i).equals(user.getState())) {
                        state_selected = states.get(i);
                        final int finalI = i;
                        spinnerState.post(new Runnable() {
                            @Override
                            public void run() {
                                spinnerState.setSelection(finalI);
                            }
                        });
                        break;
                    }
                }

                List<String> cities = CitiesHelp.getCities(toList(getResources().getStringArray(R.array.array_state_city)), state_selected);
                for (int i = 0; i < cities.size(); i++) {
                    if (cities.get(i).equals(user.getCity())) {
                        final int finalI = i;
                        spinnerCity.post(new Runnable() {
                            @Override
                            public void run() {
                                spinnerCity.setSelection(finalI);
                            }
                        });
                        break;
                    }
                }
            }
        }

//        spinnerProfile.setSelection(user.getProfile());
    }

    private List<String> toList(final String[] valueArray) {

        final List<String> valueList = new LinkedList<>(Arrays.asList(valueArray));

        valueList.add(0, getString(R.string.select));

        return valueList;
    }

    private void loadView() {

        editTextBirthDate.addTextChangedListener(Mask.insert("##/##/####", editTextBirthDate));

        spinnerGender.setAdapter(new ItemAdapter(this, toList(getResources().getStringArray(R.array.gender_array))));
        spinnerRace.setAdapter(new ItemAdapter(this, toList(getResources().getStringArray(R.array.race_array))));
        spinnerParent.setAdapter(new ItemAdapter(this, toList(getResources().getStringArray(R.array.relationship_array))));

        spinnerParent.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                findViewById(R.id.linear_layout_parent_other).setVisibility(
                        (spinnerParent.getSelectedItem().equals("Otros")) ? View.VISIBLE : View.GONE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        final List<Country> countryList = Helper.loadCountry();

        countryList.add(0, new Country(0, "", getString(R.string.select)));

        Collections.sort(countryList);
        spinnerAdapter = new CountryAdapter(this, CountryAdapter.Type.STANDARD, countryList);
        spinnerCountry.setAdapter(spinnerAdapter);

        spinnerCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(final AdapterView<?> adapterView, final View view, final int position, final long id) {
                String codeCountrySelected = countryList.get(position).getCode();

                textViewState.setVisibility(codeCountrySelected.equals(COLOMBIA) ? View.VISIBLE : View.GONE);
                spinnerState.setVisibility(codeCountrySelected.equals(COLOMBIA) ? View.VISIBLE : View.GONE);
                textViewCity.setVisibility(codeCountrySelected.equals(COLOMBIA) ? View.VISIBLE : View.GONE);
                spinnerCity.setVisibility(codeCountrySelected.equals(COLOMBIA) ? View.VISIBLE : View.GONE);
            }

            @Override
            public void onNothingSelected(final AdapterView<?> adapterView) {

            }
        });

        co.gov.ins.guardianes.view.ItemAdapter itemAdapter = new co.gov.ins.guardianes.view.ItemAdapter(this, CitiesHelp.getState(
                toList(getResources().getStringArray(R.array.array_state_city))));
        spinnerState.setAdapter(itemAdapter);

        final UserActivity this_activity = this;

        spinnerState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String state = spinnerState.getSelectedItem().toString();
                co.gov.ins.guardianes.view.ItemAdapter adapter = new co.gov.ins.guardianes.view.ItemAdapter(this_activity, CitiesHelp.getCities(
                        toList(getResources().getStringArray(R.array.array_state_city)), state));
                spinnerCity.setAdapter(adapter);
                spinnerCity.setSelection(adapter.getPosition(user.getCity()));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        if (user != null && user.getState() != null) {
            spinnerState.setSelection(itemAdapter.getPosition(user.getState()));
        }
        if (main) {

            textViewMessage.setText(R.string.message_fields);

            findViewById(R.id.linear_layout_parent).setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.button_save)
    public void onAdd() {
        add();
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            onBackPressed();

        } else {

            super.onOptionsItemSelected(item);
        }

        return true;
    }

    private void add() {
        user.setFirstname(editTextFirstname.getText().toString());
        user.setLastname(editTextLastname.getText().toString());
        user.setGender(spinnerGender.getSelectedItem().toString());
        user.setDob(editTextBirthDate.getText().toString().trim());

        final String race = spinnerRace.getSelectedItem().toString();
        user.setRace(race);

        final String country = ((Country) spinnerCountry.getSelectedItem()).getName();

        user.setCountry(country);

        if (country.equals(getString(R.string.colombia))) {
            user.setState(spinnerState.getSelectedItem().toString());
            user.setCity(spinnerCity.getSelectedItem().toString());
        } else {
            user.setState("N/A");
            user.setCity("N/A");
        }


        final String parent = spinnerParent.getSelectedItem().toString();
        user.setRelationship((parent.equalsIgnoreCase("Otros")) ? editTextParent.getText().toString() : parent);

        if (validate(user)) {
            user.setDob(DateFormat.getDate(user.getDob()));
            Log.d("USER /user/update", user.toString());
            final String url = main ? "/user/update" : create ? "/household/create" : "/household/update";

            String id = "";
            if (!main) {
                User mainUser = new PrefManager(UserActivity.this).get(Constants.Pref.USER, User.class);
                if (mainUser != null) {
                    id = mainUser.getId();
                }
            }

            new UserRequester(this).addOrUpdate(url, user, main ? null : id, new RequestHandler<String>(this) {

                @Override
                public void onError(final Exception e) {
                    super.onError(e);

                    new DialogBuilder(UserActivity.this).load()
                            .title(R.string.attention)
                            .content(R.string.error_add_new_member)
                            .positiveText(R.string.ok)
                            .show();
                }

                @Override
                public void onSuccess(final String message) {
                    super.onSuccess(message);
                    if (main) {
                        loadMain();
                    } else {
                        new DialogBuilder(UserActivity.this).load()
                                .title(R.string.attention)
                                .content(create ? R.string.member_added : R.string.member_updated)
                                .positiveText(R.string.ok)
                                .onPositive(new MaterialDialog.SingleButtonCallback() {

                                    @Override
                                    public void onClick(@NonNull final MaterialDialog dialog, @NonNull final DialogAction which) {
                                        navigateTo(HomeActivity.class);
                                    }

                                }).show();
                    }

                }
            });
        }
    }

    private void updateUser() {

        if (user != null) {

            new PrefManager(UserActivity.this).put(Constants.Pref.USER, user);
        }
    }

    private void loadMain() {

        new AuthRequester(this).loadAuth(new RequestListener<User>() {

            @Override
            public void onStart() {

            }

            @Override
            public void onError(final Exception e) {

            }

            @Override
            public void onSuccess(final User user) {

                updateUser();

                new DialogBuilder(UserActivity.this).load()
                        .title(R.string.attention)
                        .content(create ? R.string.member_added : R.string.member_updated)
                        .positiveText(R.string.ok)
                        .onPositive(new MaterialDialog.SingleButtonCallback() {

                            @Override
                            public void onClick(@NonNull final MaterialDialog dialog, @NonNull final DialogAction which) {
                                navigateTo(HomeActivity.class);
                            }

                        }).show();
            }
        });
    }

    private boolean validate(final User user) {
        Log.d("UserActivity", main ? "TRUE" : "FALSE");
        return isFirstNameValid(user.getFirstname())
                && isLastNameValid(user.getLastname())
                && isGenderValid(user.getGender())
                && (main || isRelationshipValid(user.getRelationship()))
                && isBirthDateValid(user.getDob())
                && isRaceValid(user.getRace())
                && isHomeLocationValid(user.getCountry(), user.getState(), user.getCity());
    }


    private boolean isFirstNameValid(String firstName) {

        boolean isFirstNameValid = true;

        if (firstName.isEmpty()) {
            new DialogBuilder(UserActivity.this).load()
                    .title(R.string.attention)
                    .content(R.string.empty_first_name_alert)
                    .positiveText(R.string.ok)
                    .show();
            isFirstNameValid = false;
        }

        return isFirstNameValid;
    }

    private boolean isLastNameValid(String lastName) {

        boolean isLastNameValid = true;

        if (lastName.isEmpty()) {
            new DialogBuilder(UserActivity.this).load()
                    .title(R.string.attention)
                    .content(R.string.empty_last_name_alert)
                    .positiveText(R.string.ok)
                    .show();
            isLastNameValid = false;
        }

        return isLastNameValid;
    }


    private boolean isGenderValid(String gender) {

        boolean isGenderValid = !gender.equals(getString(R.string.select));

        if (!isGenderValid) {
            new DialogBuilder(UserActivity.this).load()
                    .title(R.string.attention)
                    .content(R.string.empty_gender_alert)
                    .positiveText(R.string.ok)
                    .show();
        }

        return isGenderValid;
    }

    private boolean isBirthDateValid(String birthDate) {

        boolean isBirthDateValid = true;

        if (!DateFormat.isDate(birthDate)) {
            new DialogBuilder(UserActivity.this).load()
                    .title(R.string.attention)
                    .content(R.string.invalid_birth_date_alert)
                    .positiveText(R.string.ok)
                    .show();
            isBirthDateValid = false;
        } else if (main && DateFormat.getDateDiff(birthDate) < 13) {
            new DialogBuilder(UserActivity.this).load()
                    .title(R.string.attention)
                    .content(R.string.low_birth_date_alert)
                    .positiveText(R.string.ok)
                    .show();
            isBirthDateValid = false;
        }

        return isBirthDateValid;
    }

    private boolean isRelationshipValid(String relationship) {
        boolean isRelationshipValid = !relationship.isEmpty() && !relationship.equals(getString(R.string.select));

        if (!isRelationshipValid) {
            new DialogBuilder(UserActivity.this).load()
                    .title(R.string.attention)
                    .content(R.string.empty_relationship_alert)
                    .positiveText(R.string.ok)
                    .show();
        }

        return isRelationshipValid;
    }

    private boolean isHomeLocationValid(String country, String state, String city) {
        boolean isHomeLocationValid = true;

        if (country.equals(getString(R.string.colombia))) {
            isHomeLocationValid = isStateValid(state) && isCityValid(city);
        }

        return isHomeLocationValid;
    }

    private boolean isStateValid(String state) {
        boolean isStateValid = !state.equals(getString(R.string.select));

        if (!isStateValid) {
            new DialogBuilder(UserActivity.this).load()
                    .title(R.string.attention)
                    .content(R.string.empty_state_alert)
                    .positiveText(R.string.ok)
                    .show();
        }

        return isStateValid;
    }

    private boolean isCityValid(String city) {

        boolean isCityValid = !city.equals(getString(R.string.select));

        if (!isCityValid) {
            new DialogBuilder(UserActivity.this).load()
                    .title(R.string.attention)
                    .content(R.string.empty_city_alert)
                    .positiveText(R.string.ok)
                    .show();
        }

        return isCityValid;
    }

    private boolean isRaceValid(String race) {
        boolean isRaceValid = !race.equals(getString(R.string.select));

        if (!isRaceValid) {
            new DialogBuilder(UserActivity.this).load()
                    .title(R.string.attention)
                    .content(R.string.empty_race_alert)
                    .positiveText(R.string.ok)
                    .show();
        }

        return isRaceValid;
    }

    private void setCreate(final boolean create) {
        this.create = create;
    }
}
