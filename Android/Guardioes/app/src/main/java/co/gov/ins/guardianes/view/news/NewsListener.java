package co.gov.ins.guardianes.view.news;

import co.gov.ins.guardianes.model.News;

interface NewsListener {

    void onNewsSelect(News news);
}
