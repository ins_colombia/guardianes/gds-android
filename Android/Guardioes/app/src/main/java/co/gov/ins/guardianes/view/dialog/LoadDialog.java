package co.gov.ins.guardianes.view.dialog;

import android.os.Bundle;

import co.gov.ins.guardianes.R;

import co.gov.ins.guardianes.view.base.dialog.BaseDialogFragment;

public class LoadDialog extends BaseDialogFragment {

    public static final String TAG = LoadDialog.class.getSimpleName();

    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);

        setStyle(STYLE_NORMAL, R.style.LoadDialog);

        setCancelable(false);
    }

    @Override
    public int getLayout() {
        return R.layout.dialog_load;
    }
}
