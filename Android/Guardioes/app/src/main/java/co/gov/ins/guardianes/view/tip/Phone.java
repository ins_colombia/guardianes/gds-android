package co.gov.ins.guardianes.view.tip;

import co.gov.ins.guardianes.R;
import co.gov.ins.guardianes.view.IMenu;

/**
 * @author Igor Morais
 */
public enum Phone implements IMenu {

    EMERGENCY(1, R.string.emergency, "123"),
    FIREMAN(2, R.string.fireman, "119"),
    CIVIL_DEFENSE(3, R.string.civil_defense, "144"),
    MEDICAL_EMERGENCY(4, R.string.medical_emergency, "125"),
    OVERSIGHT(5, R.string.oversight, "122"),
    GAULA(6, R.string.gaula, "165"),
    METROPOLITAN_POLICE(7, R.string.metropolitan_police, "112"),
    ROAD_POLICE(8, R.string.road_police, "127");

    private final int id;
    private final int name;
    private final String number;

    Phone(final int id, final int name, final String number) {
        this.id = id;
        this.name = name;
        this.number = number;
    }

    @Override
    public final int getId() {
        return id;
    }

    @Override
    public final int getName() {
        return name;
    }

    @Override
    public int getIcon() {
        return 0;
    }

    @Override
    public String getTag() {
        return null;
    }

    @Override
    public boolean isDialogFragment() {
        return false;
    }

    @Override
    public boolean isFragment() {
        return false;
    }

    @Override
    public boolean isActivity() {
        return false;
    }

    @Override
    public Class<?> getType() {
        return null;
    }


    public final String getNumber() {
        return number;
    }

    public static Phone getBy(final long id) {

        for (final Phone phone : Phone.values()) {

            if (phone.getId() == id) {
                return phone;
            }
        }

        throw new IllegalArgumentException("The Phone has not found.");
    }
}
