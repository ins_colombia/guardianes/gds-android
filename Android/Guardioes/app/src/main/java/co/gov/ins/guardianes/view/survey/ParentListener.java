package co.gov.ins.guardianes.view.survey;

/**
 * @author Igor Morais
 */
public interface ParentListener {

    void onParentSelect(String id);


}
