package co.gov.ins.guardianes.view.tip;

import android.os.Bundle;

import co.gov.ins.guardianes.R;
import co.gov.ins.guardianes.view.base.BaseAppCompatActivity;
import com.google.android.gms.analytics.HitBuilders;

public class GastrointestinalActivity extends BaseAppCompatActivity {

    @Override
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);

        setContentView(R.layout.gastrointestinal);
    }

    @Override
    public void onResume() {
        super.onResume();

        getTracker().setScreenName("GastrointestinalActivity Screen - " + this.getClass().getSimpleName());
        getTracker().send(new HitBuilders.ScreenViewBuilder().build());
    }
}
