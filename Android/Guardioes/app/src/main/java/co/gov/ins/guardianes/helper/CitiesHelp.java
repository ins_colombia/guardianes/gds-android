package co.gov.ins.guardianes.helper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class CitiesHelp {

    public static List<String> getState(List<String> states_cities){
        ArrayList<String> states = new ArrayList<>();
        int index = 0;
        states.add("");
        for (int i = 1; i < states_cities.size(); i++)
        {
            String auxiliar_country = states_cities.get(i).split("-")[0];
            if(!states.get(index).equalsIgnoreCase(auxiliar_country)){
                states.add(++index, auxiliar_country);
            }
        }
        states.remove("");
        Collections.sort(states);
        states.add(0, states_cities.get(0).split("-")[0]);
        return states;
    }

    public static List<String> getCities(List<String> countries_cities, String countrie) {
        ArrayList<String> cities = new ArrayList<>();
        for (int i = 1; i < countries_cities.size(); i++)
        {
            String auxiliar_country = countries_cities.get(i).split("-")[0];
            String auxiliar_city = countries_cities.get(i).split("-")[1];
            if(auxiliar_country.equalsIgnoreCase(countrie)){
                cities.add(auxiliar_city);
            }
        }
        Collections.sort(cities);
        cities.add(0, countries_cities.get(0).split("-")[0]);
        return cities;
    }
}
