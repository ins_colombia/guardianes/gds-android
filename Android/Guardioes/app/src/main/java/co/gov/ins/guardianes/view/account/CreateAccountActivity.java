package co.gov.ins.guardianes.view.account;

import android.app.Dialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;
import co.gov.ins.guardianes.R;
import co.gov.ins.guardianes.helper.CitiesHelp;
import co.gov.ins.guardianes.helper.Constants;
import co.gov.ins.guardianes.helper.DateFormat;
import co.gov.ins.guardianes.helper.DialogBuilder;
import co.gov.ins.guardianes.helper.Helper;
import co.gov.ins.guardianes.helper.Mask;
import co.gov.ins.guardianes.manager.PrefManager;
import co.gov.ins.guardianes.model.Country;
import co.gov.ins.guardianes.model.Gender;
import co.gov.ins.guardianes.model.User;
import co.gov.ins.guardianes.push.HashReceiver;
import co.gov.ins.guardianes.push.RegisterService;
import co.gov.ins.guardianes.request.UserRequester;
import co.gov.ins.guardianes.request.base.RequestListener;
import co.gov.ins.guardianes.view.CountryAdapter;
import co.gov.ins.guardianes.view.ItemAdapter;
import co.gov.ins.guardianes.view.base.BaseAppCompatActivity;
import co.gov.ins.guardianes.view.dialog.LoadDialog;
import co.gov.ins.guardianes.view.survey.StateActivity;
import co.gov.ins.guardianes.view.welcome.TermActivity;

public class CreateAccountActivity extends BaseAppCompatActivity {

    private static final int REQUEST_MAIL = 6669;

    private static final int NONE = 0;

    private static final String COLOMBIA = "CO";

    private static final String SOCIAL_FRAGMENT = "social_fragment";
    private final LoadDialog loadDialog = new LoadDialog();

    @Bind(R.id.linear_layout_social_account)
    LinearLayout llSocial;

    @Bind(R.id.linear_layout_account)
    ScrollView llAccount;

    @Bind(R.id.edit_text_mail)
    EditText etEmail;

    @Bind(R.id.edit_text_password)
    EditText etPassword;

    @Bind(R.id.edit_text_name)
    EditText etFirstName;

    @Bind(R.id.edit_text_nickname)
    EditText etLastName;

    @Bind(R.id.edit_text_birth_date)
    EditText etBirthDate;

    @Bind(R.id.text_view_state)
    TextView tvState;

    @Bind(R.id.text_view_city)
    TextView tvCity;

    @Bind(R.id.text_view_race)
    TextView tvRace;

    @Bind(R.id.spinner_race)
    Spinner spRace;

    @Bind(R.id.spinner_gender)
    Spinner spGender;

    @Bind(R.id.spinner_country)
    Spinner spCountry;

    @Bind(R.id.spinner_city)
    Spinner spCity;

    @Bind(R.id.spinner_state)
    Spinner spState;

    private SocialFragment socialFragment;
    private State state = State.SOCIAL;
    private User user = new User();
    private CountryAdapter spinnerAdapter;
    private Country colombia = null;
    private HashReceiver receiver = new HashReceiver() {

        public void onHash(final String hash) {

            user.setFirstname(etFirstName.getText().toString().trim());
            user.setLastname(etLastName.getText().toString().trim());
            user.setDob(etBirthDate.getText().toString().trim().toLowerCase());

            final String gender = Gender.getBy(spGender.getSelectedItemPosition()).getValue();
            user.setGender(gender);

            final String race = spRace.getSelectedItem().toString();
            user.setRace(race);
            final String country = ((Country) spCountry.getSelectedItem()).getName();
            user.setCountry(country);
            if (!((Country) spCountry.getSelectedItem()).getCode().equals(COLOMBIA)) {
                user.setState(getString(R.string.na));
                user.setCity(getString(R.string.na));
            } else {
                user.setState(spState.getSelectedItem().toString());
                user.setCity(spCity.getSelectedItem().toString());
            }
//            user.setProfile(spinnerProfile.getSelectedItemPosition());
            user.setEmail(etEmail.getText().toString().trim().toLowerCase());

            if (user.getTw() != null || user.getFb() != null || user.getGl() != null) {
                user.setPassword(user.getEmail());

            } else {

                user.setPassword(etPassword.getText().toString().trim());
            }

            user.setGcmToken(hash);
            new UserRequester(CreateAccountActivity.this).createAccount(user, new RequestListener<User>() {

                @Override
                public void onStart() {

                }

                @Override
                public void onError(final Exception e) {
                    loadDialog.dismiss();
                    try {
                        if (e.getMessage().equalsIgnoreCase("User already exists")) {
                            new DialogBuilder(CreateAccountActivity.this).load()
                                    .title(R.string.attention)
                                    .content("User already exists")
                                    .positiveText(R.string.ok)
                                    .show();
                        } else {
                            Toast.makeText(CreateAccountActivity.this, R.string.erro_new_user, Toast.LENGTH_LONG).show();
                        }
                    } catch (Exception ex) {
                        Toast.makeText(CreateAccountActivity.this, R.string.erro_new_user, Toast.LENGTH_LONG).show();
                    }
                    e.printStackTrace();
                }

                @Override
                public void onSuccess(final User type) {
                    loadDialog.dismiss();

                    final Bundle bundle = new Bundle();

                    user.setUserToken(type.getUserToken());
                    user.setVersionBuild(type.getVersionBuild());
                    user.setType(type.getType());
                    user.setRelationship(type.getRelationship());
                    user.setPath(type.getPath());
                    user.setId(type.getId());
                    user.setDob(type.getDob());
                    new PrefManager(CreateAccountActivity.this).put(Constants.Pref.USER, user);

                    bundle.putBoolean(Constants.Bundle.MAIN_MEMBER, true);

                    navigateTo(StateActivity.class, Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK, bundle);
                }
            });
        }
    };

    @Override
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.create_account);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        load();

        getSocialFragment().setListener(new AccountHandler());

        for (Country obj : spinnerAdapter.getContries()) {
            if (obj.getName().equalsIgnoreCase("Colombia"))
                colombia = obj;
        }

        spCountry.post(new Runnable() {
            @Override
            public void run() {
                spCountry.setSelection(spinnerAdapter.getPosition(colombia));
            }
        });
    }

    private List<String> toList(final String[] valueArray) {

        final List<String> valueList = new LinkedList<>(Arrays.asList(valueArray));

        valueList.add(0, getString(R.string.select));

        return valueList;
    }

    private void load() {

        etBirthDate.addTextChangedListener(Mask.insert("##/##/####", etBirthDate));

        spGender.setAdapter(new ItemAdapter(this, toList(getResources().getStringArray(R.array.gender_array))));

        spRace.setAdapter(new ItemAdapter(this, toList(getResources().getStringArray(R.array.race_array))));

        spState.setAdapter(
                new ItemAdapter(this, CitiesHelp.getState(
                        toList(getResources().getStringArray(R.array.array_state_city)))));

        final CreateAccountActivity this_activity = this;

        spState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String country = spState.getSelectedItem().toString();
                spCity.setAdapter(new ItemAdapter(this_activity, CitiesHelp.getCities(
                        toList(getResources().getStringArray(R.array.array_state_city)),
                        country)));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spCity.setAdapter(new ItemAdapter(this_activity, CitiesHelp.getCities(
                toList(getResources().getStringArray(R.array.array_state_city)),
                spState.getSelectedItem().toString())));

        final List<Country> countryList = Helper.loadCountry();

        Collections.sort(countryList);

        spinnerAdapter = new CountryAdapter(this, CountryAdapter.Type.STANDARD, countryList);

        spCountry.setAdapter(spinnerAdapter);

        spCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(final AdapterView<?> adapterView, final View view, final int position, final long id) {
                String codeCountrySelected = countryList.get(position).getCode();
                if (codeCountrySelected.equals(COLOMBIA)) {
                    tvState.setVisibility(View.VISIBLE);
                    spState.setVisibility(View.VISIBLE);
                    tvCity.setVisibility(View.VISIBLE);
                    spCity.setVisibility(View.VISIBLE);
                } else {
                    tvState.setVisibility(View.GONE);
                    spState.post(new Runnable() {
                        @Override
                        public void run() {
                            spState.setSelection(NONE);
                        }
                    });
                    spState.setVisibility(View.GONE);
                    tvCity.setVisibility(View.GONE);
                    spCity.post(new Runnable() {
                        @Override
                        public void run() {
                            spCity.setSelection(NONE);
                        }
                    });
                    spCity.setVisibility(View.GONE);
                }

            }

            @Override
            public void onNothingSelected(final AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.privacy, menu);

        return true;
    }

    @Override
    public void onResume() {


        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter(HashReceiver.HASH_RECEIVER));

        getTracker().setScreenName("Create Account Screen - " + this.getClass().getSimpleName());
        getTracker().send(new HitBuilders.ScreenViewBuilder().build());
        super.onResume();
    }

    @Override
    protected void onPause() {

        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);

        super.onPause();
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {

        if (item.getItemId() == android.R.id.home) {

            if (state == State.SOCIAL) {
                return super.onOptionsItemSelected(item);

            } else {

                handlerState();
            }

        } else {

            return super.onOptionsItemSelected(item);
        }

        return true;
    }

    public void onPrivacy(final MenuItem item) {

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.privacy);
        ImageButton dialogButton = (ImageButton) dialog.findViewById(R.id.image_button_close);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    @Override
    public void onBackPressed() {

        if (state == State.SOCIAL) {
            super.onBackPressed();

        } else {
            handlerState();
        }
    }

    private void previous() {
        state = State.getBy(state.getId() - 1);
    }

    private void next() {
        state = State.getBy(state.getId() + 1);
    }

    private void handlerState() {

        if (state == State.ACCOUNT) {
            onPreviousAnimation(llSocial, llAccount);
        }
    }

    @OnClick(R.id.button_mail)
    public void onMail() {

        getTracker().send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("Create Account by Email Button")
                .build());

        navigateForResult(TermActivity.class, REQUEST_MAIL);
    }

    private void onPreviousAnimation(final View visibleView, final View invisibleView) {

        final Animation slideIn = AnimationUtils.loadAnimation(this, R.anim.slide_in_right);

        slideIn.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(final Animation animation) {
                visibleView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(final Animation animation) {
                previous();

                invisibleView.setVisibility(View.INVISIBLE);

                invisibleView.clearAnimation();
            }

            @Override
            public void onAnimationRepeat(final Animation animation) {

            }
        });

        final Animation slideOut = AnimationUtils.loadAnimation(this, R.anim.slide_out_right);

        slideOut.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(final Animation animation) {

            }

            @Override
            public void onAnimationEnd(final Animation animation) {
                visibleView.clearAnimation();
            }

            @Override
            public void onAnimationRepeat(final Animation animation) {

            }
        });

        visibleView.startAnimation(slideIn);

        invisibleView.startAnimation(slideOut);
    }

    private SocialFragment getSocialFragment() {

        if (socialFragment == null) {
            socialFragment = (SocialFragment) getFragmentManager().findFragmentByTag(SOCIAL_FRAGMENT);
        }

        return socialFragment;
    }

    @OnClick(R.id.button_create_account)
    public void onCreateAccount() {

        if (state == State.ACCOUNT && areAllFieldsValid()) {
            loadDialog.show(getFragmentManager(), LoadDialog.TAG);
            startService(new Intent(CreateAccountActivity.this, RegisterService.class));
        }
    }

    public void setUser(final User user) {
        this.user = user;
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent intent) {

        if (requestCode == REQUEST_MAIL) {

            if (resultCode == RESULT_OK) {

                next();

                llSocial.setVisibility(View.INVISIBLE);
                llAccount.setVisibility(View.VISIBLE);
            }

        } else {

            getSocialFragment().onActivityResult(requestCode, resultCode, intent);
        }
    }

    private enum State {

        SOCIAL(1), ACCOUNT(2);

        private final int id;

        State(final int id) {
            this.id = id;
        }

        public static State getBy(final int id) {

            for (final State state : State.values()) {

                if (state.getId() == id) {
                    return state;
                }
            }

            throw new IllegalArgumentException("The State has not found.");
        }

        public int getId() {
            return id;
        }
    }

    private class AccountHandler implements AccountListener {

        @Override
        public void onCancel() {

        }

        @Override
        public void onError() {

        }

        @Override
        public void onNotFound(final User user) {

            next();

            etLastName.setText(user.getLastname());
            etEmail.setText(user.getEmail());

            llSocial.setVisibility(View.INVISIBLE);
            llAccount.setVisibility(View.VISIBLE);

            findViewById(R.id.text_layout_password).setVisibility(View.INVISIBLE);

            setUser(user);
        }

        @Override
        public void onSuccess(final User user) {

            new DialogBuilder(CreateAccountActivity.this).load()
                    .title(R.string.attention)
                    .content(R.string.already_registered)
                    .positiveText(R.string.ok)
                    .show();
        }
    }

    private boolean areAllFieldsValid() {

        return isFirstNameValid()
                && isLastNameValid()
                && isGenderValid()
                && isBirthDateValid()
                && isHomeLocationValid()
                && isRaceValid()
                && isEmailValid()
                && isPasswordValid();
    }


    private boolean isFirstNameValid() {
        String firstName = etFirstName.getText().toString();

        boolean isFirstNameValid = true;

        if (firstName.isEmpty()) {
            new DialogBuilder(CreateAccountActivity.this).load()
                    .title(R.string.attention)
                    .content(R.string.empty_first_name_alert)
                    .positiveText(R.string.ok)
                    .show();
            isFirstNameValid = false;
        }

        return isFirstNameValid;
    }

    private boolean isLastNameValid() {
        String lastName = etLastName.getText().toString();

        boolean isLastNameValid = true;

        if (lastName.isEmpty()) {
            new DialogBuilder(CreateAccountActivity.this).load()
                    .title(R.string.attention)
                    .content(R.string.empty_last_name_alert)
                    .positiveText(R.string.ok)
                    .show();
            isLastNameValid = false;
        }

        return isLastNameValid;
    }


    private boolean isGenderValid() {

        boolean isGenderValid = spGender.getSelectedItemPosition() != NONE;

        if (!isGenderValid) {
            new DialogBuilder(CreateAccountActivity.this).load()
                    .title(R.string.attention)
                    .content(R.string.empty_gender_alert)
                    .positiveText(R.string.ok)
                    .show();
        }

        return isGenderValid;
    }

    private boolean isBirthDateValid() {

        String birthDate = etBirthDate.getText().toString().trim().toLowerCase();

        boolean isBirthDateValid = true;

        if (!DateFormat.isDate(birthDate)) {
            new DialogBuilder(CreateAccountActivity.this).load()
                    .title(R.string.attention)
                    .content(R.string.invalid_birth_date_alert)
                    .positiveText(R.string.ok)
                    .show();
            isBirthDateValid = false;
        } else if(DateFormat.getDateDiff(birthDate) < 13) {
            new DialogBuilder(CreateAccountActivity.this).load()
                    .title(R.string.attention)
                    .content(R.string.low_birth_date_alert)
                    .positiveText(R.string.ok)
                    .show();
            isBirthDateValid = false;
        }

        return isBirthDateValid;
    }

    private boolean isHomeLocationValid() {
        boolean isHomeLocationValid = true;

        if (((Country) spCountry.getSelectedItem()).getCode().equals(COLOMBIA)) {
            isHomeLocationValid = isStateValid() && isCityValid();
        }

        return isHomeLocationValid;
    }

    private boolean isStateValid() {
        boolean isStateValid = spState.getSelectedItemPosition() != NONE;

        if (!isStateValid) {
            new DialogBuilder(CreateAccountActivity.this).load()
                    .title(R.string.attention)
                    .content(R.string.empty_state_alert)
                    .positiveText(R.string.ok)
                    .show();
        }

        return isStateValid;
    }

    private boolean isCityValid() {

        boolean isCityValid = spCity.getSelectedItemPosition() != NONE;

        if (!isCityValid) {
            new DialogBuilder(CreateAccountActivity.this).load()
                    .title(R.string.attention)
                    .content(R.string.empty_city_alert)
                    .positiveText(R.string.ok)
                    .show();
        }

        return isCityValid;
    }

    private boolean isRaceValid() {
        boolean isRaceValid = spRace.getSelectedItemPosition() != NONE;

        if (!isRaceValid) {
            new DialogBuilder(CreateAccountActivity.this).load()
                    .title(R.string.attention)
                    .content(R.string.empty_race_alert)
                    .positiveText(R.string.ok)
                    .show();
        }

        return isRaceValid;
    }

    private boolean isEmailValid() {
        String email = etEmail.getText().toString().toLowerCase();

        boolean isEmailValid = true;

        if (email.isEmpty()) {
            new DialogBuilder(CreateAccountActivity.this).load()
                    .title(R.string.attention)
                    .content(R.string.empty_email_alert)
                    .positiveText(R.string.ok)
                    .show();
            isEmailValid = false;
        } else {
            try {
                if (email.split("@")[1].split("\\.").length < 2) {
                    isEmailValid = false;
                }
            } catch (Exception e) {
                isEmailValid = false;
            }

            if (!isEmailValid) {
                new DialogBuilder(CreateAccountActivity.this).load()
                        .title(R.string.attention)
                        .content(R.string.invalid_email_alert)
                        .positiveText(R.string.ok)
                        .show();
            }
        }

        return isEmailValid;
    }

    private boolean isPasswordValid() {
        String password = etPassword.getText().toString();

        boolean isPasswordValid = true;

        if (password.length() < 6) {
            new DialogBuilder(CreateAccountActivity.this).load()
                    .title(R.string.attention)
                    .content(R.string.invalid_password_alert)
                    .positiveText(R.string.ok)
                    .show();
            isPasswordValid = false;
        }

        return isPasswordValid;
    }
}
