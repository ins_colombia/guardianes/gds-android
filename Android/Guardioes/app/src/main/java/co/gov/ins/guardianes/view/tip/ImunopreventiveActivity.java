package co.gov.ins.guardianes.view.tip;

import android.os.Bundle;

import co.gov.ins.guardianes.R;
import co.gov.ins.guardianes.view.base.BaseAppCompatActivity;
import com.google.android.gms.analytics.HitBuilders;


public class ImunopreventiveActivity  extends BaseAppCompatActivity {

    @Override
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);

        setContentView(R.layout.imunoprentive);
    }

    @Override
    public void onResume() {
        super.onResume();

        getTracker().setScreenName("ImunopreventiveActivity Screen - " + this.getClass().getSimpleName());
        getTracker().send(new HitBuilders.ScreenViewBuilder().build());
    }
}
