package co.gov.ins.guardianes.helper;

import android.content.Context;
import android.widget.ImageView;

import co.gov.ins.guardianes.R;
import co.gov.ins.guardianes.model.User;

public class AvatarHelper {

    public void loadImage(final Context context, final ImageView view, final User user) {

        if (user.getGender().equals(context.getString(R.string.male))) {
            view.setImageResource(R.drawable.avatar_default_male);
        } else if (user.getGender().equals(context.getString(R.string.female))) {
            view.setImageResource(R.drawable.avatar_default_female);
        } else {
            view.setImageResource(R.drawable.perfil_avatar);
        }
    }

}
