package co.gov.ins.guardianes.view;

/**
 * @author Igor Morais
 */
public interface MenuListener {

    void onMenuSelect(IMenu menu);
}
